#/usr/bin/make

.PHONY: all clean install 

SRC = $(DESTDIR)/usr/src
SHARE = $(DESTDIR)/usr/share/$(NAME)-dkms
ROOT_DIR := $(shell pwd)

all:

clean:

install:

#source tree
	install -d "$(SRC)/$(NAME)-$(VERSION)"
	cp -a compat-wireless $(SRC)/$(NAME)-$(VERSION)/.
	cp dkms.conf $(SRC)/$(NAME)-$(VERSION)/.
	cp dkms-makefile $(SRC)/$(NAME)-$(VERSION)/Makefile
	cp build-wlan.sh $(SRC)/$(NAME)-$(VERSION)/.
	chmod a+r -R "$(SRC)/$(NAME)-$(VERSION)"
	sed -i 's/__VERSION__/$(VERSION)/g' "$(SRC)/$(NAME)-$(VERSION)"/dkms.conf

#tarball, possibly with binaries
ifeq ("$(wildcard $(NAME)-$(VERSION).dkms.tar.gz)", "$(NAME)-$(VERSION).dkms.tar.gz")
	install -d "$(SHARE)"
	install -m 644 $(NAME)-$(VERSION).dkms.tar.gz "$(SHARE)"
endif

#postinst
	install -d "$(SHARE)"
	install -m 755 $(PREFIX)/usr/lib/dkms/common.postinst $(SHARE)/postinst

